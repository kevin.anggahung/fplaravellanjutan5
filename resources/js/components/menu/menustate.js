import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        order: {}
    },
    getters: {
        getOrder: state => {
            return state.menu
        }
    },
    mutations: {
        add(menu) {
            if (state.order[menu]) {
                state.order[menu]++
            } else {
                state.order[menu] = 0
            }

        },
        minus(menu) {
            if (state.order[menu]) {
                state.order[menu]--
            }
            if (state.order[menu] == 0) {
                state.order = state.order.filter((item, key, index) => {
                    return index !== menu
                })
            }

        },
    }
})