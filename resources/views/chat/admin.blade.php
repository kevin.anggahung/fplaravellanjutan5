@extends('layouts.app')

@section('content')
<div class="container">
    <admin-chat-component id="chatbox"></admin-chat-component>
</div>
@endsection

@push('scripts')
<script>
</script>
@endpush
