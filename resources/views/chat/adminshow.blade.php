@extends('layouts.app')

@section('content')
<div class="container">
    <chat-box-component id="chatbox"></chat-box-component>
</div>
@endsection

@push('scripts')
<script>
    document.getElementById('chatbox').setAttribute('recievername', "{{$reciever->name}}")
    document.getElementById('chatbox').setAttribute('recieverid', "{{$reciever->id}}")
</script>
@endpush
