@extends('layouts.app')

@section('content')
<div class="container">
    <chat-box-component id="chatbox"></chat-box-component>
</div>
@endsection

@push('scripts')
<script>
    document.getElementById('chatbox').setAttribute('recievername', window.Laravel.admin.name)
    document.getElementById('chatbox').setAttribute('recieverid', window.Laravel.admin.id)
</script>
@endpush
