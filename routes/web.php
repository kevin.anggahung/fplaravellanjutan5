<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::group([

    'middleware' => 'auth'
  
  ], function ($router) {
    
    //Chat
    Route::get('/chat', 'ChatController@showChatBox');
    Route::get('/getadmin' , 'ChatController@getAdmin');
    Route::post('/chat-messages' , 'ChatController@getChat');
    Route::get('/all-chat-messages' , 'ChatController@getAllChat');
    Route::post('/chat' , 'ChatController@postChat');

    //Menu
    Route::view('/', 'menu.index')->name('home');
    Route::get('/menu/all-menus' , 'MenuController@index');
    Route::post('/proceed', 'TransactionController@create');
  });

  Route::group([

    'middleware' => ['auth', 'role:1']
  
  ], function ($router) {
    //transaction record
    Route::get('/transaction', 'TransactionController@index');
    Route::get('/transaction/{id}', 'TransactionController@show');

    //admin chat
    Route::get('/chatadmin', 'ChatController@showAdminIndex');
    Route::get('/chatadmin/{id}', 'ChatController@showAdminChatBox');

  });
