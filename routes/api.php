<?php

use Illuminate\Support\Facades\Route;

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth',
    'namespace' => 'Auth'

], function ($router) {

  Route::post('register' , 'RegisterAPIController');
  Route::post('login' , 'LoginAPIController');
  Route::post('logout' , 'LogoutAPIController');

});

Route::group([

  'middleware' => 'auth:api',
  'prefix' => 'menu',

], function ($router) {

  Route::get('/' , 'MenuController@index');
  Route::post('/' , 'MenuController@store');
  Route::post('/delete/{id}' , 'MenuController@delete');
  Route::post('/update/{id}' , 'MenuController@update');

});

Route::group([

  'middleware' => 'auth'

], function ($router) {

  Route::get('/chat-messages' , 'ChatController@getChat');
  Route::get('/all-chat-messages' , 'ChatController@getAllChat');
  Route::post('/chat' , 'ChatController@postChat');

});

Route::group([

  'middleware' => 'auth:api',
  'prefix' => 'menu',

], function ($router) {

  Route::get('/' , 'TransactionController@index');
  Route::post('/' , 'TransactionController@store');
  Route::post('/delete/{id}' , 'TransactionController@delete');
  Route::post('/update/{id}' , 'TransactionController@update');

});


