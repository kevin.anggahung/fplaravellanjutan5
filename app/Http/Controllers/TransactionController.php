<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(){
        $transactions = Transaction::all();
        return view('transaction.index', compact('transactions'));
    }

    public function show($id){
        $transaction = Transaction::find($id);
        return view('transaction.show', compact('transaction'));
    }

    public function create(Request $request){
        $transaction = auth()->user()->transactions()->create([
            'total_price' => $request->total_price,
            'method' => $request->method
        ]);
        foreach ($request->detail as $value) {
            $transaction->transaction_details()->create([
                'menu_id' => $value['menu']['id'],
                'quantity' => $value['count']
            ]);
        }
        return $transaction;
    }
}
