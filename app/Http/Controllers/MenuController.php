<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Http\Requests\MenuRequest;
use App\Http\Resources\MenuResource;

class MenuController extends Controller
{
    public function index(){

        $menus = Menu::orderBy('menu_name')->get();

        return MenuResource::collection($menus);
    }

    public function store(MenuRequest $request){

      $menu = Menu::create([
        'menu_name' => $request->menu_name,
        'description' => $request->description,
        'price' => $request->price
      ]);

      return new MenuResource($menu);
    }

    public function delete($id){
      Menu::destroy($id);
      return 'success';
    }

    public function update($id, MenuRequest $request){
      $menu = Menu::find($id);

      $menu->update([
        'menu_name' => $request->menu_name,
        'description' => $request->description,
        'price' => $request->price
      ]);

      return new MenuResource($menu);
    }
}

