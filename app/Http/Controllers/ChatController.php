<?php

namespace App\Http\Controllers;

use App\Events\ChatSentEvent;
use App\Models\Chat;
use App\Models\User;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showChatBox(){
        $admin = User::where('role_id', 1)->get()->first();
        return view('chat.user', compact('admin'));
    }

    public function showAdminIndex(){
        $chats = Chat::all();
        $temp = [];
        foreach ($chats as $chat) {
            if($chat->sender_id != auth()->user()->id){
                $temp[$chat->sender_id] = $chat->sender->name;
            }else if ($chat->reciever_id != auth()->user()->id){
                $temp[$chat->reciever_id] = $chat->sender->name;
            }
        }
        $chats = $temp;
        return view('chat.admin', compact('chats'));
    }


    public function showAdminChatBox($id){
        $reciever = User::find($id);
        return view('chat.adminshow', compact('reciever'));
    }

    public function getAdmin(){
        return User::where('role_id', 1)->get()->first();
    }

    public function getChat(Request $request){
        $recieved = Chat::where('sender_id', $request->target_id)
                        ->where('reciever_id', auth()->user()->id)
                        ->get();
        $sent = Chat::where('sender_id', auth()->user()->id)
                        ->where('reciever_id', $request->target_id)
                        ->get();
        return compact('recieved', 'sent');
    }

    public function getAllChat(){
        return Chat::all();
    }
    
    public function postChat(Request $request){
        
        $chat = auth()->user()->sent_chats()->create([
            'subject' => $request->subject,
            'reciever_id' => $request->reciever_id
        ]);
        broadcast(new ChatSentEvent($chat))->toOthers();
        return $chat;
    }
}
