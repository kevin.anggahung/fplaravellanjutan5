<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role_target=0)
    {
        if(auth()->user()->role_id < $role_target)
            abort(401, "this route is only for admin");
        return $next($request);
    }
}
